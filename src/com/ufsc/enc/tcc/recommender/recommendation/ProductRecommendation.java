/**
 * 
 */
package com.ufsc.enc.tcc.recommender.recommendation;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ufsc.enc.tcc.recommender.control.MarketRequest;
import com.ufsc.enc.tcc.recommender.database.AuxiliaryStructureBasis;
import com.ufsc.enc.tcc.recommender.database.InteractionBasis;
import com.ufsc.enc.tcc.recommender.database.MetaInformationBasis;
import com.ufsc.enc.tcc.recommender.database.MongoDB;
import com.ufsc.enc.tcc.recommender.database.RecommendationBasis;
import com.ufsc.enc.tcc.recommender.model.EPC;
import com.ufsc.enc.tcc.recommender.model.FrequencyMatrix;
import com.ufsc.enc.tcc.recommender.model.SimilarityMatrix;
import com.ufsc.enc.tcc.recommender.util.Constants;
import com.ufsc.enc.tcc.recommender.util.GeneralOperations;

/**
 * @author trdp
 *
 */
public class ProductRecommendation {
	private MongoDB mongoDB;

	public ProductRecommendation(MongoDB mongoDB) {
		this.mongoDB = mongoDB;
	}

	/**************************************************************************************
	 * 
	 * @param idFridge
	 *************************************************************************************/
	public void recommendMissingProduct(Integer idFridge, Integer numRecProducts) {
		List<JSONObject> recommendationList = new ArrayList<>();

		/*********************************************
		 * Instanciação das bases
		 *********************************************/
		// MetaInformationBasis metaInformationBasis = new
		// MetaInformationBasis(this.mongoDB); // Bases que serão manipuladas
		InteractionBasis interactionBasis = new InteractionBasis(this.mongoDB);
		AuxiliaryStructureBasis auxiliaryStructureBasis = new AuxiliaryStructureBasis(this.mongoDB);

		Timestamp tmp = new Timestamp(Constants.MINIMUM_HOURS_OUTSIDE * 3600 * 1000); // Timestamp com tempo em horas
																						// para ser considerado como
																						// presente (manter assim, sem
																						// usar a função 'General...'

		/*********************************************
		 * Resgate dos produtos contidos atualmente
		 *********************************************/
		List<JSONObject> listInteractions = interactionBasis.getInteractionsFridge(idFridge, tmp); // Resgata as
																									// interações que
																									// ocorreram dentro
																									// do intervalo de
																									// tempo estipulado
																									// pelo Timestamp
		JSONObject jsonCurProds = GeneralOperations.getCurrentproductsByInteractions(listInteractions, this.mongoDB); // Listar
																														// produtos
																														// contidos
																														// atualmente
																														// partir
																														// da
																														// lista
																														// de
																														// interações

		/*********************************************
		 * Resgata as configurações da geladeira
		 *********************************************/
		JSONObject settings = auxiliaryStructureBasis.getSettings(idFridge); // Resgata as configurações da geladeira

		System.out.println("---------------------------------------\nSettings\t" + settings.toString() + "\t" + idFridge
				+ "\n---------------------------------------\n");

		try {
			/************************************************************
			 * Comparação entre produtos requisitados e produtos contidos
			 ************************************************************/
			JSONArray requisitedProducts = settings.getJSONArray(Constants.PRODUCT_LIST_KEY); // Produto requisitos pelo
																								// usuário
			JSONArray existingProducts = jsonCurProds.getJSONArray(Constants.PRODUCT_LIST_KEY); // Produtos contidos
																								// atualmente

			System.out.println("\n---------------------------------------\n");
			System.out.println("Requisitados\t" + requisitedProducts.toString());
			System.out.println("Existentes\t" + existingProducts.toString());
			System.out.println("\n---------------------------------------\n");

			JSONArray missingProducts = new JSONArray();

			for (int i = 0; i < requisitedProducts.length(); i++) { // Para cada produto requisitado.
				boolean requisitedExists = false; // Será verificado se ele está contido.

				/*********************************************
				 * Resgate info do produto requisitado atual
				 *********************************************/
				JSONObject jsonRequisited = requisitedProducts.getJSONObject(i); // Produto atual para comparação

				String barcodeReq = jsonRequisited.getString(Constants.PRODUCT_BARCODE_KEY); // Código do produto
																								// requisitado
				Double quantReq = Double.valueOf(jsonRequisited.getString(Constants.PRODUCT_QUANTITY_KEY)); // Quantidade
																											// requisitado
																											// do
																											// produto.

				/*********************************************************************
				 * Procura dentro da lista de existente: - Se há o produto - E se a quantidade
				 * existente está de acordo com o requisitado
				 *********************************************************************/
				for (int j = 0; j < existingProducts.length(); j++) {

					// para cada produto contido,
					JSONObject jsonExisting = existingProducts.getJSONObject(j);
					String barcodeExis = jsonExisting.getString(Constants.PRODUCT_BARCODE_KEY);

					/******************************************************************
					 * Para cada produto contido, verifica se é o mesmo do requisitado
					 ******************************************************************/
					if (barcodeReq.equals(barcodeExis)) { // Verifica-se a igualdade dos códigos de barra

						Double quantExis = Double.valueOf(jsonExisting.getString(Constants.PRODUCT_QUANTITY_KEY)); // Resgate
																													// da
																													// quantidade
																													// requisitada

						/******************************************************************
						 * Verifica se a quantidade existente é menor que o esperado. - Se for, adiciona
						 * à lista de produtos faltantes
						 ******************************************************************/
						if (quantExis < quantReq) { // Comparação entre qtd requisitada e existente
							System.out.println("Em falta: " + barcodeReq + "\t\tQtd: " + (quantReq - quantExis));
							JSONObject jsonMissing = new JSONObject();

							jsonMissing.put(Constants.PRODUCT_BARCODE_KEY, barcodeReq);
							jsonMissing.put(Constants.PRODUCT_QUANTITY_KEY, (quantReq - quantExis));
							missingProducts.put(jsonMissing);

						}

						requisitedExists = true; // Informa que o produto requisitado está contido na geladeira,
													// independentemente de a qtd ser suficiente ou não.
					}
				}

				/***************************************************************************************
				 * Caso o produto não esteja contido, - é automaticamente inserido na lista de
				 * faltantes
				 ***************************************************************************************/
				if (!requisitedExists) {
					System.out.println("Não existe: " + barcodeReq);

					JSONObject jsonMissing = new JSONObject();

					jsonMissing.put(Constants.PRODUCT_BARCODE_KEY, barcodeReq);
					jsonMissing.put(Constants.PRODUCT_QUANTITY_KEY, quantReq);

					missingProducts.put(jsonMissing);
				}
			}
			System.out.println("---------------------------------------\n");
			System.out.println("Missing products:\t\t" + missingProducts.toString());
			System.out.println("---------------------------------------\n");
			/***************************************************************************************
			 * Caso não existe produto em falta - Simplemente retorna lista nula.
			 ***************************************************************************************/
			if (missingProducts.length() == 0) { // Se tiver OK, não faz nada e retorna null
				System.out.println("Sem produtos em falta");
				return;
			}

			/***************************************************************************************
			 * Para cada produto na lista de faltante, Mostra o código de barras e a
			 * quantidade que falta.
			 ***************************************************************************************/
			for (int i = 0; i < missingProducts.length(); i++) {
				JSONObject jsonProd = missingProducts.getJSONObject(i);
				System.out.println("---------------------------------------\n");
				System.out.println(
						"Procurando no mercado: \n\tProd:\t" + jsonProd.getString(Constants.PRODUCT_BARCODE_KEY)
								+ "\n\tQtd:\t" + jsonProd.getDouble(Constants.PRODUCT_QUANTITY_KEY));
				System.out.println("---------------------------------------\n");
			}

			// Se não tiver:

			// * Pesquisa cada produto no mercado pra ver se tá disponível

			// Para cada produto verifica disponibilidade no mercado

			// Se tiver disponível, retorna adiciona na lista de compras
			// Se não:
			// Calcula o ângulo desse produto em relação a cada outro
			// Calcula o ângulo com outros usuários e aquele que melhor corresponder

			/***************************************************************************************
			 * Resgate das configurações de mercado
			 ***************************************************************************************/
			JSONObject marketSettings = settings.getJSONObject(Constants.MARKET_KEY);// Parâmetros do mercado

			/***************************************************************************************
			 * Verificação de disponibilidade de cada produto faltante.
			 ***************************************************************************************/
			JSONArray resultAvailability = MarketRequest.verifyDisponibility(idFridge, missingProducts, marketSettings); // Verificação
																															// de
																															// disponibilidade
																															// dos
																															// produtos
																															// no
																															// mercado.
			JSONObject arrayObject = new JSONObject();
			arrayObject.put(Constants.PRODUCT_LIST_KEY, missingProducts);

			/***************************************************************************************
			 * Criação de HashMap para otimizar acesso aos itens.
			 ***************************************************************************************/
			HashMap<String, Double> missingProductsMap = GeneralOperations.productListToHashMap(arrayObject);
			// HashMap<String, Double> recommendedProdBarcodes = new HashMap<>();

			/***************************************************************************************
			 * Para cada produto na lista resultante da requisição ao mercado, - Se tiver
			 * disponível coloca na lista de recomendação - Se não, procura produtos
			 * semelhante e adiciona como alternativa
			 ***************************************************************************************/
			for (int i = 0; i < resultAvailability.length(); i++) {

				JSONObject product = resultAvailability.getJSONObject(i);

				Integer status = product.getInt(Constants.STATUS_AVAILABILITY_KEY);
				String barCode = product.getString(Constants.PRODUCT_BARCODE_KEY);

				//////////////////////////////////////////////////
				if (status == Constants.STATUS_AVAILABLE) {
					// recommendedProdBarcodes.put(barcode, missingProductsMap.get(barcode));
					JSONObject jsonOriginalAv = new JSONObject();

					jsonOriginalAv.put(Constants.PRODUCT_BARCODE_KEY, barCode);

					Double value = missingProducts.getJSONObject(i).getDouble(Constants.PRODUCT_QUANTITY_KEY);
					jsonOriginalAv.put(Constants.PRODUCT_QUANTITY_KEY, value);
					jsonOriginalAv.put(Constants.RECOMMENDATION_TYPE_KEY, Constants.PRODUCT_REC_OLD);

					recommendationList.add(jsonOriginalAv);
				} else {
					// Recomenda semelhante
					/////////////////////////////////////////////////////////////////

					/***************************************************************************************
					 * Recomendação de produtos semelhante ao indicado pelo código de barras
					 ***************************************************************************************/
					List<String> barCodeSimilar = getSimilarProduct(barCode);

					if (barCodeSimilar != null) {
						// TODO: Não tem recomendação
						System.out.println("No similar to " + barCode);

						JSONArray similar = new JSONArray();

						/***************************************************************************************
						 * Para cada produto similar, - Coloca na lista de produtos
						 ***************************************************************************************/
						System.out.println(missingProductsMap);

						for (int j = 0; j < barCodeSimilar.size(); j++) {
							JSONObject similarProd = new JSONObject();

							String simcode = barCodeSimilar.get(j);
							Double simqtt = missingProductsMap.get(barCode);

							similarProd.put(Constants.PRODUCT_BARCODE_KEY, simcode);
							similarProd.put(Constants.PRODUCT_QUANTITY_KEY, simqtt); // TODO: problema aqui

							similar.put(similarProd);
						}

						/***************************************************************************************
						 * Verificação de disponibilidade dos produtos semelhantes
						 ***************************************************************************************/
						JSONArray similarAvailability = MarketRequest.verifyDisponibility(idFridge, similar,
								marketSettings);

						/***************************************************************************************
						 * Verificação de quais produtos estão disponíveis
						 ***************************************************************************************/
						for (int j = 0; j < similarAvailability.length(); j++) {
							JSONObject similarAv = similarAvailability.getJSONObject(j);

							/***************************************************************************************
							 * Caso o produto similar esteja disponível, cria-se um JSON com info do
							 * original e do alternativo e coloca na lista de recomendação
							 ***************************************************************************************/
							if (similarAv.getInt(Constants.STATUS_AVAILABILITY_KEY) == Constants.STATUS_AVAILABLE) {

								JSONObject jsonAlternative = new JSONObject();

								JSONObject jsonAlternativeData = new JSONObject();
								JSONObject jsonOriginalData = new JSONObject();

								// Dados do produto alternativo
								jsonAlternativeData.put(Constants.PRODUCT_BARCODE_KEY,
										similarAv.getString(Constants.PRODUCT_BARCODE_KEY));

								// Dados do produto original
								jsonOriginalData.put(Constants.PRODUCT_BARCODE_KEY, barCode);
								double qtt = similar.getJSONObject(j).getDouble(Constants.PRODUCT_QUANTITY_KEY);

								jsonAlternative.put(Constants.ALTERNATIVE_PRODUCT, jsonAlternativeData);
								jsonAlternative.put(Constants.ORIGINAL_PRODUCT, jsonOriginalData);
								jsonAlternative.put(Constants.PRODUCT_QUANTITY_KEY, qtt);
								jsonAlternative.put(Constants.RECOMMENDATION_TYPE_KEY, Constants.PRODUCT_REC_SIMILAR);

								recommendationList.add(jsonAlternative);

								System.out.println("ALTERNATIVE_REC:\t" + jsonAlternative);

								break;
							}

							/***************************************************************************************
							 * Caso contrário, indica que nenhuma alternativa ao produto original existe
							 * 
							 ***************************************************************************************/
							else {
								JSONObject jsonAlternative = new JSONObject();
								JSONObject jsonOriginalData = new JSONObject();

								jsonOriginalData.put(Constants.PRODUCT_BARCODE_KEY, barCode);
								double qtt = similar.getJSONObject(j).getDouble(Constants.PRODUCT_QUANTITY_KEY);

								jsonAlternative.put(Constants.ORIGINAL_PRODUCT, jsonOriginalData);
								jsonAlternative.put(Constants.PRODUCT_QUANTITY_KEY, qtt);
								jsonAlternative.put(Constants.RECOMMENDATION_TYPE_KEY, Constants.PRODUCT_REC_SIMILAR);

								System.out.println("NO_ALTERNATIVE_REC:\t" + jsonAlternative);

								recommendationList.add(jsonAlternative);
							}
						}
					}
				}
			}

			/***************************************************************************************
			 * Salva na base
			 ***************************************************************************************/
			System.out.println("---------------------------------------\n");
			System.out.println("Lista de recomendação de produtos:\n\n" + recommendationList + "\n\n");
			System.out.println("---------------------------------------\n");

			RecommendationBasis recommendationBasis = new RecommendationBasis(this.mongoDB);
			recommendationBasis.insertProductRecommendation(idFridge, Constants.PRODUCT_REC_MISS, recommendationList); // TODO
																														// Rever
																														// se
																														// salva
																														// um
																														// por
																														// um
																														// ou
																														// o
																														// conjunto
																														// inteiro
		} catch (JSONException e) {
			e.printStackTrace();
			return;
		}

		// Retorna pro usuário.

		return;
	}

	/********************************************************************************************************************************/

	/**
	 * Recomenda produtos semelhantes a partir da abordagem baseada em conteúdo
	 * 
	 * @param barCode
	 * @return
	 */
	private List<String> getSimilarProduct(String barCode) {
		MetaInformationBasis metaInformationBasis = new MetaInformationBasis(this.mongoDB);
		System.out.println(metaInformationBasis.getAllProductMetaInfo());
		JSONObject productMetaInfo = metaInformationBasis.getProductMetaInfo(barCode);

		try {
			JSONObject classification = new JSONObject().put(Constants.PRODUCT_CLASSIFICATION_KEY,
					productMetaInfo.getJSONObject(Constants.PRODUCT_CLASSIFICATION_KEY));

			System.out.println("Produto Original:\t" + productMetaInfo.toString());
			List<String> listSimilar = metaInformationBasis.getProductsMetaInfoByClass(classification);

			while (listSimilar.remove(barCode)) // pode ter mais de um do mesmo código
				;

			// listSimilar.remove(barCode); // Remove o produto original dentro da lista da
			// mesma classe.
			if (listSimilar.size() == 0) {
				return null;
			}

			return listSimilar;
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * 
	 */
	public FrequencyMatrix mountFrequencyMatrix() {
		FrequencyMatrix frequencyMatrix = new FrequencyMatrix(); // Matriz de interações

		InteractionBasis interactionBasis = new InteractionBasis(this.mongoDB); // Base de interações
		MetaInformationBasis metaInformationBasis = new MetaInformationBasis(this.mongoDB);

		List<JSONObject> interactions = interactionBasis.listAllInteractions(); // Todas as interações

		// Interactions
		for (JSONObject interaction : interactions) { // Para cada interação
			try {
				Integer userID = interaction.getInt(Constants.ID_FRIDGE_KEY); // Pega o id do user
				JSONArray epcs = interaction.getJSONArray(Constants.EPC_CODES_KEY); // Pega o array de EPC

				for (int j = 0; j < epcs.length(); j++) { // Para cada EPC
					String epc = epcs.getString(j); // Pega a string do EPC
					BigInteger epcBI = new BigInteger(epc);
					EPC epcObj = new EPC(epcBI);

					String barcode = metaInformationBasis.getProductBarcodeByEPC(epcObj); // Pega o código de barra
																							// relacionado ao EPC
					frequencyMatrix.put(userID, barcode); // Adiciona à matriz
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		System.out.println("MATRIZ: " + frequencyMatrix.toString());
		// frequencyMatrix.getAverageFrequencies(this.mongoDB);

		return frequencyMatrix;
	}

	public void recommendNewProduct() {
		// Descobrir o id máximo de usuário
		// Montar matriz de frequência -> Mf
		FrequencyMatrix freqMatrix = this.mountFrequencyMatrix();
		System.out.println(freqMatrix);
		// Calcular as médias de avaliação
		HashMap<Integer, Double> averages = freqMatrix.getAverageFrequencies(this.mongoDB);

		// Integer countUsers = averages.keySet().size();
		SimilarityMatrix sMatrix = new SimilarityMatrix(freqMatrix, averages, this.mongoDB);
		HashMap<Integer, HashMap<Integer, Double>> anglesMatrix = sMatrix.getSimilarityMatrix();
		System.out.println(anglesMatrix);
		// System.out.println(
		// "///////////////////////////////////////////////////////////////////////////////////////////////////////////");

		for (Integer userId1 : anglesMatrix.keySet()) { // Para cada usuário na matrix

			HashMap<Integer, Double> angulos = anglesMatrix.get(userId1);
			angulos = (HashMap<Integer, Double>) GeneralOperations.sortByValueDescending(angulos);
			ArrayList<String> produtos = new ArrayList<>();

			System.out.println("Angulos: " + userId1 + " |||||| " + angulos);

			HashMap<String, JSONObject> products = new HashMap<>();

			int count = 0;

			for (Integer user2 : angulos.keySet()) {

				/*
				 * if (count > 20) { break; }
				 */

				// System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
				Set<String> prodsU1 = new HashSet<String>(freqMatrix.getUserProductsFreq(userId1).keySet());
				// System.out.println(prodsU1);
				Set<String> prodsU2 = new HashSet<String>(freqMatrix.getUserProductsFreq(user2).keySet());
				// System.out.println(prodsU2);
				prodsU2.removeAll(prodsU1);
				produtos.addAll(prodsU2);
				// se o tamanho do vetor de produtos final for maior que x, cai fora.

				// System.out.println(freqMatrix.getUserProductsFreq(userId1).keySet());
				// System.out.println("LISTA:" + produtos);

				if (produtos.size() > 0) {

					// JSONArray jArrProd = new JSONArray(produtos);

					MetaInformationBasis metaInformationBasis = new MetaInformationBasis(this.mongoDB);

					for (int i = 0; i < produtos.size(); i++) {
						JSONObject json = metaInformationBasis.getProductMetaInfo(produtos.get(i));

						try {
							products.put(json.getString(Constants.PRODUCT_BARCODE_KEY), json);
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}

				}
			}

			RecommendationBasis recommendationBasis = new RecommendationBasis(this.mongoDB);

			ArrayList<JSONObject> productMetainfo = new ArrayList<>();
			for (String key : products.keySet()) {
				productMetainfo.add(products.get(key));
			}

			recommendationBasis.insertProductRecommendation(userId1, Constants.PRODUCT_REC_NEW, productMetainfo);

		}
	}
}