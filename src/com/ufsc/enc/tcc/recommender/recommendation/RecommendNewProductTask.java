/**
 * 
 */
package com.ufsc.enc.tcc.recommender.recommendation;

import java.util.TimerTask;

import org.json.JSONException;
import org.json.JSONObject;

import com.ufsc.enc.tcc.recommender.database.MongoDB;
import com.ufsc.enc.tcc.recommender.database.RecommendationBasis;
import com.ufsc.enc.tcc.recommender.util.GeneralOperations;

/**
 * @author trdp
 *
 */
public class RecommendNewProductTask extends TimerTask
{
	private MongoDB mongoDB;

	public RecommendNewProductTask(MongoDB mongoDB)
	{
		this.mongoDB = mongoDB;
	}

	@Override
	public void run()
	{
		Long t1 = System.nanoTime();
		
		ProductRecommendation productRecommendation = new ProductRecommendation(this.mongoDB);
		productRecommendation.recommendNewProduct();
		
		Long t2 = System.nanoTime();
		GeneralOperations.showTime(t2 - t1);
	}
}