/**
 * 
 */
package com.ufsc.enc.tcc.recommender.recommendation;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.bson.Document;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.ufsc.enc.tcc.recommender.database.AuxiliaryStructureBasis;
import com.ufsc.enc.tcc.recommender.database.InteractionBasis;
import com.ufsc.enc.tcc.recommender.database.MetaInformationBasis;
import com.ufsc.enc.tcc.recommender.database.MongoDB;
import com.ufsc.enc.tcc.recommender.database.RecommendationBasis;
import com.ufsc.enc.tcc.recommender.model.Classification;
import com.ufsc.enc.tcc.recommender.model.FrequencyMatrix;
import com.ufsc.enc.tcc.recommender.util.Constants;
import com.ufsc.enc.tcc.recommender.util.DataBases;
import com.ufsc.enc.tcc.recommender.util.GeneralOperations;
import com.ufsc.enc.tcc.recommender.util.JSONOperations;

/**
 * @author trdp
 *
 */
public class RecipeRecommendation {

	private MongoDB mongoDB;

	public RecipeRecommendation(MongoDB mongoDB) {
		this.mongoDB = mongoDB;
	}

	/**
	 * Pegar receitas pelo conteúdo.
	 * 
	 * @param currentProducts
	 * @param metaInformationBasis
	 * @return
	 */
	private HashMap<JSONObject, Integer> getRecipesFromContent(HashMap<String, Double> currentProducts,
			MetaInformationBasis metaInformationBasis) {
		HashMap<Classification, Double> hashMapContentClass = new HashMap<>();

		// Montagem do Hash para produtos contidos
		for (String barCode : currentProducts.keySet()) {

			List<Document> document = mongoDB.search(Constants.PRODUCT_BARCODE_KEY, barCode, // Pesquisa na base pelo
																								// código de barras
					DataBases.META_INFO_BASE_NAME);

			if (document.size() == 0) { // Caso não tenha encontrado nenhum produto
				continue; // Pula pro próximo código de barras
			}

			List<JSONObject> jsonDoc = JSONOperations.documentListToJSONObjectList(document); // MetaInfo do produto que
																								// tem o código de
																								// barras especificado

			try {

				JSONObject jsonMetaInfo = jsonDoc.get(0).getJSONObject(Constants.PRODUCT_CLASSIFICATION_KEY); // Pega o
																												// primeiro
																												// (supostamente
																												// único)
																												// da
																												// lista

				Classification classification = new Classification();
				classification.set_especification(jsonMetaInfo.getInt(Constants.PRODUCT_ESPECIFICATION_KEY));
				classification.set_class(jsonMetaInfo.getInt(Constants.PRODUCT_CLASS_KEY));

				if (hashMapContentClass.containsKey(classification)) { // Caso já contenha a chave do código de barars
					Double count = hashMapContentClass.get(classification);
					hashMapContentClass.put(classification, ((Double) currentProducts.get(barCode)) + count); // Adiciona
																												// a
																												// quantidade
																												// do
																												// produto
																												// atual
																												// à
																												// classe
																												// que
																												// ele
																												// pertence
				} else { // Caso não contenha a chave
					hashMapContentClass.put(classification, currentProducts.get(barCode)); // Adiciona a quantidade do
																							// produto atual diretamente
																							// na classe
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		// Pega todas as as receitas
		List<JSONObject> listAllRecipe = metaInformationBasis.getAllRecipeMetaInfo();
		List<JSONObject> matchRecipes = new ArrayList<>();
		HashMap<JSONObject, Integer> matchRecipesHash = new HashMap<>();

		// Para cada receita
		for (JSONObject recipeJSON : listAllRecipe) {
			Integer countMatches = 0;

			try {
				JSONArray recipeProducts = recipeJSON.getJSONArray(Constants.PRODUCT_LIST_KEY);

				if (recipeProducts == null) {
					continue;
				}

				System.out.println("RECIPESS");

				for (int j = 0; j < recipeProducts.length(); j++) { // Para cada produto da receita

					System.out.println(recipeProducts.getJSONObject(j));
					JSONObject prodClass = recipeProducts.getJSONObject(j)
							.getJSONObject(Constants.PRODUCT_CLASSIFICATION_KEY);

					Classification classification = new Classification();
					classification.set_especification(prodClass.getInt(Constants.PRODUCT_ESPECIFICATION_KEY));
					classification.set_class(prodClass.getInt(Constants.PRODUCT_CLASS_KEY));

					if (hashMapContentClass.containsKey(classification)) { // Verificar se a sua classificação coincide
																			// com alguma da lista.
						countMatches++; // Caso sim, conta como um match.
					}
				}

				if (countMatches >= Constants.MINIMUM_MATCHES_PRODUCT_RECIPE) { // Caso tenha pelo menos dois produtos
					// matchRecipes.add(recipeJSON); // Adiciona a receita à lista de receitas que
					// fecham com o conteúdo.
					matchRecipesHash.put(recipeJSON, countMatches);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		if (matchRecipesHash.isEmpty()) {
			return null;
		}

		return matchRecipesHash;
	}

	/**
	 * Recomendação de receitas com base no conteúdo a disposição na geladeira.
	 * 
	 * @param idFridge
	 */
	public void recommendRecipeByContent(Integer idFridge) {
		@SuppressWarnings("unused")
		ArrayList<JSONObject> recommendationList = new ArrayList<>();

		InteractionBasis interactionBasis = new InteractionBasis(this.mongoDB);
		// AuxiliaryStructureBasis auxiliaryStructureBasis = new
		// AuxiliaryStructureBasis(this.mongoDB);
		MetaInformationBasis metaInformationBasis = new MetaInformationBasis(this.mongoDB);
		RecommendationBasis recommendationBasis = new RecommendationBasis(mongoDB);

		Timestamp tmp = GeneralOperations.getHoursAgoTimestamp(Constants.MINIMUM_HOURS_OUTSIDE);
		ArrayList<JSONObject> listInteractions = new ArrayList<JSONObject>(
				interactionBasis.getInteractionsFridge(idFridge, tmp));

		// Selecionar os produtos atuais na geladeira
		HashMap<String, Double> currentProd = GeneralOperations.getCurrentproductsByInteractionsHash(listInteractions,
				this.mongoDB);

		System.out.println("CurrentProd:\t" + currentProd);

		// Pegar todas as receitas que tenham pelo menos X itens constantes na geladeira
		HashMap<JSONObject, Integer> hashMatches = this.getRecipesFromContent(currentProd, metaInformationBasis); // Pegar
																													// as
																													// receitas
																													// que
																													// tem
																													// pelo
																													// menos
																													// dois
																													// produtos
																													// batendo.

		System.out.println("Matches: " + hashMatches);
		GeneralOperations.sortByValueDescending(hashMatches); // Ordem pelo valor de cada chave.

		ArrayList<JSONObject> listaRec = new ArrayList<>();
		int count = 0;
		// Guardar na base
		for (JSONObject jsonRecipe : hashMatches.keySet()) {

			if (count > Constants.MAX_REC_RECIPE) {
				break;
			}

			listaRec.add(jsonRecipe);
		}
		System.out.println("---------------\nREC:\n" + listaRec);

		recommendationBasis.insertRecipeRecommendation(idFridge, listaRec);
	}

	/**
	 * 
	 * @param idFridge
	 */
	public void recommendRecipeByProfile(Integer idFridge) {
		// Resgatar o histórico
		InteractionBasis interactionBasis = new InteractionBasis(this.mongoDB);
		MetaInformationBasis metaInformationBasis = new MetaInformationBasis(this.mongoDB);
		RecommendationBasis recommendationBasis = new RecommendationBasis(this.mongoDB);

		List<JSONObject> listInt = interactionBasis.getInteractionsFridge(idFridge);
		JSONObject products = GeneralOperations.getCurrentproductsByInteractions(listInt, this.mongoDB);

		JSONArray productsArray = null;
		try {
			productsArray = products.getJSONArray(Constants.PRODUCT_LIST_KEY); // Resgate dos produtos que interagiu
		} catch (JSONException e1) {
			e1.printStackTrace();
			return;
		}

		FrequencyMatrix frequencyMatrix = new FrequencyMatrix();

		// Para cada produto interagido
		for (int i = 0; i < productsArray.length(); i++) {

			try {
				JSONObject jsonProduct = productsArray.getJSONObject(i);

				String barcode = jsonProduct.getString(Constants.PRODUCT_BARCODE_KEY);
				frequencyMatrix.put(idFridge, barcode);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		HashMap<String, Integer> frequencyProducts = frequencyMatrix.getUserProductsFreq(idFridge);

		// Ordenar pela maior frequência até a menor
		System.out.println(frequencyProducts);

		/********************************/
		Random random = new Random(System.nanoTime());
		// for (String key : frequencyProducts.keySet()) {
		// Integer x = frequencyProducts.get(key);
		// frequencyProducts.put(key, x + random.nextInt(10));
		// }
		// System.out.println(frequencyProducts);
		/**********************************/
		frequencyProducts = (HashMap<String, Integer>) GeneralOperations.sortByValueDescending(frequencyProducts);
		System.out.println(frequencyProducts);

		HashMap<String, Double> hash = new HashMap<>();

		// Selecionar os cinco produtos do topo da lista.
		int count = 0;
		for (String keyBarcode : frequencyProducts.keySet()) {

			if (count > Constants.MAX_PRODUCT_TOP)
				break;

			JSONObject jsonProd = metaInformationBasis.getProductMetaInfo(keyBarcode);
			try {
				Double quantity = jsonProd.getDouble(Constants.PRODUCT_QUANTITY_KEY);
				hash.put(keyBarcode, quantity);

				count++;
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Hash: " + hash);
		// Pegar todas as receitas que tenham pelo menos X itens constantes na geladeira
		HashMap<JSONObject, Integer> hashMatches = this.getRecipesFromContent(hash, metaInformationBasis); // Pegar as
																											// receitas
																											// que tem
																											// pelo
																											// menos
																											// dois
																											// produtos
																											// batendo.

		System.out.println("Matches: " + hashMatches);
		if (hashMatches != null) {
			hashMatches = (HashMap<JSONObject, Integer>) GeneralOperations.sortByValueDescending(hashMatches); // TODO:
																												// verifica
																												// ascendente
																												// ou
																												// descendente
																												// //
																												// Ordem
																												// pelo
																												// valor
																												// de
																												// cada
																												// chave.

			ArrayList<JSONObject> listaRec = new ArrayList<>();
			count = 0;

			// Guardar na base
			for (JSONObject jsonRecipe : hashMatches.keySet()) {

				if (count > Constants.MAX_REC_RECIPE) {
					break;
				}

				listaRec.add(jsonRecipe);
			}

			System.out.println(listaRec);
			recommendationBasis.insertRecipeRecommendation(idFridge, listaRec);
		}

		// Recomendar receitas baseadas nesse conjunto.
	}
}
