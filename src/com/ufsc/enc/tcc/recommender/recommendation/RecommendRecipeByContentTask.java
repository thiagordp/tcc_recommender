/**
 * 
 */
package com.ufsc.enc.tcc.recommender.recommendation;

import java.util.TimerTask;

import com.ufsc.enc.tcc.recommender.database.MongoDB;

/**
 * @author trdp
 *
 */
public class RecommendRecipeByContentTask extends TimerTask
{
	private MongoDB mongoDB;

	public RecommendRecipeByContentTask(MongoDB mongoDB)
	{
		this.mongoDB = mongoDB;
	}

	@Override
	public void run()
	{
		System.out.println("recipebycontent");
		RecipeRecommendation recipeRecommendation = new RecipeRecommendation(this.mongoDB);
		recipeRecommendation.recommendRecipeByContent(2);
	}
}