/**
 * 
 */
package com.ufsc.enc.tcc.recommender.recommendation;

import java.util.TimerTask;

import com.ufsc.enc.tcc.recommender.database.MongoDB;

/**
 * @author trdp
 *
 */
public class RecommendRecipeByProfileTask extends TimerTask
{

	private MongoDB mongoDB;

	public RecommendRecipeByProfileTask(MongoDB mongoDB)
	{
		super();
		this.mongoDB = mongoDB;
	}

	@Override
	public void run()
	{
		System.out.println(
				"\n----------------------------------------------\nrecipebyprofile\n--------------------------------------------\n");

		RecipeRecommendation recipeRecommendation = new RecipeRecommendation(this.mongoDB);
		recipeRecommendation.recommendRecipeByProfile(1);
	}
}
