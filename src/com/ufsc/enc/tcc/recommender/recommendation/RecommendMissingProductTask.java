/*
 *  
 */
package com.ufsc.enc.tcc.recommender.recommendation;

import java.util.TimerTask;

import com.ufsc.enc.tcc.recommender.database.MongoDB;
import com.ufsc.enc.tcc.recommender.database.RecommendationBasis;
import com.ufsc.enc.tcc.recommender.util.Constants;
import com.ufsc.enc.tcc.recommender.util.GeneralOperations;

/**
 * @author trdp
 *
 */
public class RecommendMissingProductTask extends TimerTask
{
	private MongoDB mongoDB;

	public RecommendMissingProductTask(MongoDB mongoDB)
	{
		this.mongoDB = mongoDB;
	}

	@Override
	public void run()
	{
		Long t1 = System.nanoTime();

		ProductRecommendation productRecommendation = new ProductRecommendation(this.mongoDB);

		//for (int i = 1; i <= Constants.FRIDGE_COUNT; i++) {
			productRecommendation.recommendMissingProduct(2, 3);
		//}

		Long t2 = System.nanoTime();
		System.out.print("RecTime\t");
		GeneralOperations.showTime(t2 - t1);
	}
}
