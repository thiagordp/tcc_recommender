package com.ufsc.enc.tcc.recommender.database;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.json.JSONException;
import org.json.JSONObject;

import com.ufsc.enc.tcc.recommender.util.Constants;
import com.ufsc.enc.tcc.recommender.util.DataBases;
import com.ufsc.enc.tcc.recommender.util.JSONOperations;

public class InteractionBasis
{
	private MongoDB mongoDB;

	public InteractionBasis(MongoDB mongoDB)
	{
		this.mongoDB = mongoDB;
	}

	public void insertInteraction(JSONObject interaction)
	{

		// mongoDB = new MongoDB(DataBases.DATABASE_NAME, DataBases.getCollections());
		mongoDB.insert(JSONOperations.jsonObjectToDocument(interaction), DataBases.INTERACTION_BASE_NAME);
		//	mongoDB.closeConnection();
	}

	public List<JSONObject> getInteractionsFridge(Integer idFridge)
	{

		// mongoDB = new MongoDB(DataBases.DATABASE_NAME, DataBases.getCollections());

		List<JSONObject> jsonObjects = JSONOperations.documentListToJSONObjectList(
				mongoDB.search(Constants.ID_FRIDGE_KEY, idFridge, DataBases.INTERACTION_BASE_NAME));

		//	mongoDB.closeConnection();

		return jsonObjects;
	}

	/**
	 * Retorna todas interações da geladeira com identificação idFridge que ocorrem em até X horas em relação à última interação
	 * @param idFridge
	 * @param minimumTimestamp
	 * @return
	 */
	public List<JSONObject> getInteractionsFridge(Integer idFridge, Timestamp minimumTimestamp)
	{
		System.out.println("\ngetInteractionsFridge\n-------------");
		// mongoDB = new MongoDB(DataBases.DATABASE_NAME, DataBases.getCollections());

		List<JSONObject> tmpList = JSONOperations.documentListToJSONObjectList(
				mongoDB.search(Constants.ID_FRIDGE_KEY, idFridge, DataBases.INTERACTION_BASE_NAME));
		//	mongoDB.closeConnection();
		if (tmpList.size() == 0) {
			return null;
		}
		List<JSONObject> jsonObjects = new ArrayList<JSONObject>();

		try {

			JSONObject lastInteraction = tmpList.get(tmpList.size() - 1);
			Timestamp tmtp = Timestamp.valueOf(lastInteraction.getString(Constants.TIMESTAMP_KEY));

			Timestamp compareTmtp = new Timestamp(tmtp.getTime() - minimumTimestamp.getTime());

			for (JSONObject jsonObject : tmpList) {
				try {
					String timeStmp = jsonObject.getString(Constants.TIMESTAMP_KEY);

					Timestamp currentTimestamp = Timestamp.valueOf(timeStmp);

					if (currentTimestamp.after(compareTmtp)) {
						jsonObjects.add(jsonObject);
					}
				}
				catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		catch (JSONException e1) {
			e1.printStackTrace();
			jsonObjects = null;
		}

		return jsonObjects;
	}

	public JSONObject getLastInteraction(Integer idFridge)
	{
		// mongoDB = new MongoDB(DataBases.DATABASE_NAME, DataBases.getCollections());

		List<JSONObject> jsonObjects = JSONOperations.documentListToJSONObjectList(
				mongoDB.search(Constants.ID_FRIDGE_KEY, idFridge, DataBases.INTERACTION_BASE_NAME));
		//mongoDB.closeConnection();

		if (jsonObjects == null || jsonObjects.isEmpty()) {
			return new JSONObject();
		}

		return jsonObjects.get(jsonObjects.size() - 1);
	}

	/**
	 * Remoção de todas os registros da coleção
	 * @return
	 */
	public String removeAllInteractions()
	{

		// mongoDB = new MongoDB(DataBases.DATABASE_NAME, DataBases.getCollections());
		mongoDB.removeAll(DataBases.INTERACTION_BASE_NAME);
		//mongoDB.closeConnection();

		return Constants.OK_JSON;
	}

	public List<JSONObject> listAllInteractions()
	{

		// mongoDB = new MongoDB(DataBases.DATABASE_NAME, DataBases.getCollections());
		List<Document> tmpList = mongoDB.listEntries(DataBases.INTERACTION_BASE_NAME);
		//mongoDB.closeConnection();

		return JSONOperations.documentListToJSONObjectList(tmpList);
	}
}
