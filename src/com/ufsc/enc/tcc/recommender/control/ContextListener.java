package com.ufsc.enc.tcc.recommender.control;

import java.util.Random;
import java.util.Timer;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.ufsc.enc.tcc.recommender.database.MongoDB;
import com.ufsc.enc.tcc.recommender.recommendation.RecommendMissingProductTask;
import com.ufsc.enc.tcc.recommender.recommendation.RecommendNewProductTask;
import com.ufsc.enc.tcc.recommender.recommendation.RecommendRecipeByContentTask;
import com.ufsc.enc.tcc.recommender.recommendation.RecommendRecipeByProfileTask;
import com.ufsc.enc.tcc.recommender.util.Constants;
import com.ufsc.enc.tcc.recommender.util.DataBases;

/**
 * Classe responsável por detectar a inicialização e finalização de contexto a
 * fim de engatilhar e encerrar o processo de recomendação
 * 
 * @author trdp
 */
public class ContextListener implements ServletContextListener {

	/**
	 * Temporizador da execução da tarefa
	 */
	private Timer timer1 = null;

	private Timer timer2 = null;

	private Timer timer3 = null;

	private Timer timer4 = null;

	private Timer timer5 = null;

	private MongoDB mongoDB;

	/**
	 * Tarefa de recomendação que é executada periocamente
	 */
	private RecommendMissingProductTask recommendMissingProductTask;

	private RecommendNewProductTask recommendNewProductTask;

	private RecommendRecipeByContentTask recommendRecipeByContentTask;

	private RecommendRecipeByProfileTask recommendRecipeByProfileTask;

	/**
	 * Verificação de inicialização de contexto
	 * 
	 * @param sce
	 *            Evento de contexto
	 */
	public void contextInitialized(ServletContextEvent sce) {
		this.mongoDB = new MongoDB(DataBases.DATABASE_NAME, DataBases.getCollections());

		this.mongoDB.removeAll(DataBases.RECOMMENDATION_BASE_NAME);
		Random random = new Random(System.nanoTime());
/*
		if (timer1 == null) {

			timer1 = new Timer();

			recommendMissingProductTask = new RecommendMissingProductTask(this.mongoDB);
			Integer x = random.nextInt(Constants.EXECUTION_TIME_INTERVAL / 5);
			Double y = x / 1000.0;
			System.out.println("Timer 1 começará em " + y + "s");
			timer1.schedule(recommendMissingProductTask, x, Constants.EXECUTION_TIME_INTERVAL);
		}
*/
		if (timer2 == null) {
			timer2 = new Timer();

			recommendNewProductTask = new RecommendNewProductTask(this.mongoDB);
			Integer x = random.nextInt(Constants.EXECUTION_TIME_INTERVAL / 100);
			Double y = x / 1000.0;
			System.out.println("Timer 2 começará em " + y + "s");
			timer2.schedule(recommendNewProductTask, x, Constants.EXECUTION_TIME_INTERVAL);

		}

		if (timer3 == null) {
			timer3 = new Timer();

			recommendRecipeByContentTask = new RecommendRecipeByContentTask(this.mongoDB);

			Integer x = random.nextInt(Constants.EXECUTION_TIME_INTERVAL / 100);
			Double y = x / 1000.0;
			System.out.println("Timer 3 começará em " + y + "s");
			timer3.schedule(recommendRecipeByContentTask, x, Constants.EXECUTION_TIME_INTERVAL);

		}

		if (timer4 == null) {
			timer4 = new Timer();

			recommendRecipeByProfileTask = new RecommendRecipeByProfileTask(this.mongoDB);
			Integer x = random.nextInt(Constants.EXECUTION_TIME_INTERVAL / 100);
			Double y = x / 1000.0;
			System.out.println("Timer 4 começará em " + y + "s");
			timer4.schedule(recommendRecipeByProfileTask, x, Constants.EXECUTION_TIME_INTERVAL);

		} /*
			 * if (timer5 == null) { timer5 = new Timer();
			 * 
			 * AutomaticPurchaseTask automaticPurchaseTask = new
			 * AutomaticPurchaseTask(mongoDB); Integer x =
			 * random.nextInt(Constants.EXECUTION_TIME_INTERVAL / 5);
			 * 
			 * Double y = x / 1000.0; System.out.println("Timer 5 come�ar� em " + y + "s");
			 * 
			 * timer5.schedule(automaticPurchaseTask, x, Constants.EXECUTION_TIME_INTERVAL);
			 * }
			 */
	}

	/**
	 * Verificação de finalização de contexto
	 * 
	 * @param sce
	 *            Evento de contexto
	 */
	public void contextDestroyed(ServletContextEvent sce) {
		this.mongoDB.closeConnection();

		try {

			timer1.cancel();
			timer2.cancel();
			timer3.cancel();
			timer4.cancel();
			timer5.cancel();

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			timer1 = null;
			timer2 = null;
			timer3 = null;
			timer4 = null;
			timer5 = null;
		}
	}
}