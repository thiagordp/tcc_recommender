/**
 * 
 */
package com.ufsc.enc.tcc.recommender.control;

import java.util.List;
import java.util.TimerTask;

import org.bson.Document;

import com.ufsc.enc.tcc.recommender.database.MongoDB;
import com.ufsc.enc.tcc.recommender.util.Constants;
import com.ufsc.enc.tcc.recommender.util.DataBases;

/**
 * @author pc
 *
 */
public class AutomaticPurchaseTask extends TimerTask {
	private MongoDB mongoDB;

	public AutomaticPurchaseTask(MongoDB mongoDB) {
		this.mongoDB = mongoDB;
	}

	@Override
	public void run() {
		System.out.println("AUTOPURCHASE");

		List<Document> list = this.mongoDB.search(Constants.RECORD_TYPE_KEY, Constants.RECORD_TYPE_PURCHASE_LIST,
				DataBases.AUX_STRUCT_BASE_NAME);

		System.out.println(list);
	}
}
