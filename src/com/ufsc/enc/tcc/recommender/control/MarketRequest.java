/**
 * 
 */
package com.ufsc.enc.tcc.recommender.control;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ufsc.enc.tcc.recommender.util.Constants;
import com.ufsc.enc.tcc.recommender.util.GeneralOperations;

/**
 * @author trdp
 *
 */
public class MarketRequest
{

	public static JSONArray verifyDisponibility(Integer idFridge, JSONArray products, JSONObject marketJSON)
	{

		try {
			String hostName = marketJSON.getString(Constants.HOSTNAME_KEY);				// ip do servidor
			Integer portNumber = marketJSON.getInt(Constants.PORT_KEY);					// porta
			String pathName = marketJSON.getString(Constants.PATH_NAME_KEY);		    // Contexto da aplicação do mercado dentro do servidor
			String resource = marketJSON.getJSONObject(Constants.RESOURCES_KEY)			// Endereço do recurso ou serviço que será utilizado.
					.getString(Constants.RESOURCE_AVAILABILITY_KEY);

			String urlStr = "http://" + hostName + ":" + String.valueOf(portNumber) + pathName + resource;		// Montagem da URL

			//System.out.println("URL\t" + urlStr);		// DEBUG: imprimir url resultante

			URL url = new URL(urlStr);														// Criação da URL
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();		// Abertura 
			urlConnection.setDoOutput(true);
			urlConnection.setRequestMethod("POST");
			urlConnection.setRequestProperty("Content-Type", "application/json");

			OutputStream os = urlConnection.getOutputStream();
			os.write(products.toString().getBytes());

			//System.out.println("Response:\t" + urlConnection.getResponseCode());

			if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
				String response = GeneralOperations.convertStreamToString(urlConnection.getInputStream());
				//System.out.println(response);

				JSONObject json = new JSONObject(response);
				JSONArray jsonArray = json.getJSONArray("products");

				return jsonArray;
			}
			else {

			}

		}
		catch (JSONException | MalformedURLException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}
}
