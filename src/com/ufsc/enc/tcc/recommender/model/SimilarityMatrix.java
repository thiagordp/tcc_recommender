package com.ufsc.enc.tcc.recommender.model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import com.ufsc.enc.tcc.recommender.database.MetaInformationBasis;
import com.ufsc.enc.tcc.recommender.database.MongoDB;

public class SimilarityMatrix {
	private FrequencyMatrix fMatrix;
	private HashMap<Integer, Double> avgs;

	private HashMap<Integer, HashMap<Integer, Double>> simMatrix;
	private MongoDB mongoDB;

	public SimilarityMatrix(FrequencyMatrix fMatrix, HashMap<Integer, Double> avgs, MongoDB mongoDB) {
		this.fMatrix = fMatrix;
		this.avgs = avgs;
		this.simMatrix = new HashMap<>();
		this.mongoDB = mongoDB;
	}

	private void calculateSimilarity(Integer userId1, Integer userId2) {
		HashMap<String, Integer> freqU1 = fMatrix.getUserProductsFreq(userId1);
		HashMap<String, Integer> freqU2 = fMatrix.getUserProductsFreq(userId2);

		Set<String> keys = new HashSet<>();

		Set<String> keys1 = freqU1.keySet();
		Set<String> keys2 = freqU2.keySet();

		keys.addAll(keys1);
		keys.addAll(keys2);

		Double numerator = 0D;
		Double denPt1 = 0D;
		Double denPt2 = 0D;

		for (String barCode : keys) {

			Double numPt1, numPt2;
			Integer f1, f2;

			f1 = freqU1.containsKey(barCode) ? freqU1.get(barCode) : 0;
			f2 = freqU2.containsKey(barCode) ? freqU2.get(barCode) : 0;

			numPt1 = f1 - avgs.get(userId1);
			numPt2 = f2 - avgs.get(userId2);

			numerator += numPt1 * numPt2;
			System.out.println("Numerador:\t" + numerator);

			denPt1 += Math.pow(numPt1, 2);
			denPt2 += Math.pow(numPt2, 2);
		}

		MetaInformationBasis metaInformationBasis = new MetaInformationBasis(this.mongoDB);
		int countProduct = metaInformationBasis.getAllProductMetaInfo().size();
		int restCount = countProduct - keys.size();

		Double numPt1 = -avgs.get(userId1);
		Double numPt2 = -avgs.get(userId2);

		numerator += numPt1 * numPt2 * restCount;
		denPt1 += Math.pow(numPt1, 2) * restCount;
		denPt2 += Math.pow(numPt2, 2) * restCount;

		System.out.println("User:\t" + userId1 + "\t" + userId2);
		System.out.println(
				"\nNumerador:\t" + numerator + "\nDenominador1:\t" + denPt1 + "\nDenominador2:\t" + denPt2 + "\n\n");

		Double sim = (numerator) / (Math.pow(denPt1, 0.5) * Math.pow(denPt2, 0.5));

		// System.out.println("CHAVES \t" + userId1 + ",\t" + userId2 + "\t:" + sim);

		if (!simMatrix.containsKey(userId1)) {
			HashMap<Integer, Double> hash = new HashMap<>();
			hash.put(userId2, sim);

			simMatrix.put(userId1, hash);
		} else {

			HashMap<Integer, Double> hash = simMatrix.get(userId1);
			hash.put(userId2, sim);
		}
		if (!simMatrix.containsKey(userId2)) {
			HashMap<Integer, Double> hash = new HashMap<>();
			hash.put(userId1, sim);

			simMatrix.put(userId2, hash);
		} else {
			HashMap<Integer, Double> hash = simMatrix.get(userId2);
			hash.put(userId1, sim);
		}
	}

	private void calculateAngles() {
		for (Integer userId1 : avgs.keySet()) {
			for (Integer userId2 : avgs.keySet()) {

				if (userId1 < userId2) {
					calculateSimilarity(userId1, userId2);
				}
			}
		}
	}

	public HashMap<Integer, HashMap<Integer, Double>> getSimilarityMatrix() {
		calculateAngles();
		return this.simMatrix;
	}
}
