package com.ufsc.enc.tcc.recommender.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import com.ufsc.enc.tcc.recommender.database.MetaInformationBasis;
import com.ufsc.enc.tcc.recommender.database.MongoDB;

public class FrequencyMatrix
{
	private HashMap<Integer, HashMap<String, Integer>> matrix;

	public FrequencyMatrix()
	{
		this.matrix = new HashMap<>();

	}

	public void put(Integer idFridge, String barCode)
	{
		if (matrix.containsKey(idFridge)) {
			HashMap<String, Integer> productsUserMap = matrix.get(idFridge);

			if (productsUserMap.containsKey(barCode)) {

				int value = productsUserMap.get(barCode) + 1;
				productsUserMap.put(barCode, value);
			}
			else {
				productsUserMap.put(barCode, 1);
			}
		}
		else {
			HashMap<String, Integer> productsUserMap = new HashMap<>();

			productsUserMap.put(barCode, 1);
			matrix.put(idFridge, productsUserMap);
		}
	}

	public HashMap<Integer, HashMap<String, Integer>> getMatrix()
	{
		return matrix;
	}

	public HashMap<String, Integer> getUserProductsFreq(Integer userId)
	{
		return matrix.get(userId);
	}

	public HashMap<Integer, Double> getAverageFrequencies(MongoDB mongoDB)
	{
		MetaInformationBasis metaInformationBasis = new MetaInformationBasis(mongoDB);

		Set<Integer> keys = this.matrix.keySet();
		HashMap<Integer, Double> averages = new HashMap<>();

		Integer count = metaInformationBasis.getAllProductMetaInfo().size();
		System.out.println("No products:\t" + count);
		for (Integer userID : keys) {

			Double sum = 0D;
			Collection<Integer> frequencies = matrix.get(userID).values();

			for (Iterator<Integer> iterator = frequencies.iterator(); iterator.hasNext();) {
				Integer frequency = (Integer) iterator.next();

				sum += frequency.doubleValue();
			}

			averages.put(userID, (sum / count));
		}

		System.out.println("AVERAGES:\t" + averages);

		return averages;
	}

	@Override
	public String toString()
	{
		return this.matrix.toString();
	}

}
