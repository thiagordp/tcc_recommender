package com.ufsc.enc.tcc.recommender.model;

public class Classification
{
	private Integer _class;
	private Integer _especification;

	@Override
	public boolean equals(Object obj)
	{
		Classification c2 = (Classification) obj;

		if (this.get_class().equals(c2.get_class()) && this.get_especification().equals(c2.get_especification()))
			return true;
		return false;
	}

	@Override
	public int hashCode()
	{
		return (1000 * _class) + _especification;
	}

	@Override
	public String toString()
	{
		return "[" + this._class + ", " + this._especification + "]";
	}

	public Integer get_class()
	{
		return _class;
	}

	public void set_class(Integer _class)
	{
		this._class = _class;
	}

	public Integer get_especification()
	{
		return _especification;
	}

	public void set_especification(Integer _especification)
	{
		this._especification = _especification;
	}

}
