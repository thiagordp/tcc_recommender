package com.ufsc.enc.tcc.recommender.util;

public class MetaInfoConstants
{
	//#####################   PRODUCTS   #####################################//

	/**
	 * Classes
	 */
	public static final Integer LATICINIO = 1;
	public static final Integer EMBUTIDO = 2;
	public static final Integer BEBIDA = 3;
	public static final Integer CONGELADO = 4;
	public static final Integer CARNE = 5;
	public static final Integer CONFEITARIA=6;
	/**
	 * Produtos
	 */

	public static final Integer LEITE = 1;
	public static final Integer MARGARINA = 2;
	public static final Integer LEITE_CONDENSADO = 3;
	public static final Integer CREME_DE_LEITE = 4;
	public static final Integer DOCE = 5;
	public static final Integer IOGURTE = 6;
	public static final Integer QUEIJO_MUSSARELA = 7;
	public static final Integer MORTADELA = 8;
	public static final Integer CERVEJA = 9;
	public static final Integer REFRIGERANTE = 10;
	public static final Integer PIZZA = 11;
	public static final Integer LINGUICA = 12;
	public static final Integer ISCAS_CARNE = 13;
	public static final Integer BIFE = 14;
	public static final Integer MAMINHA = 15;
	public static final Integer LASANHA = 16;
	public static final Integer SALSICHA = 17;
	public static final Integer FERMENTO = 18;
	public static final Integer ACUCAR = 19;

	//#####################   RECIPES   #####################################//

	/***************************************************
	 * Classes
	 ***************************************************/

	public static final Integer CAKE_PIE = 1;
	public static final Integer MEAT = 2;
	public static final Integer CANDY_DESSERT = 3;
	public static final Integer SNACK = 4;

	/***************************************************
	 * Especificação
	 ***************************************************/

	// Bolos e tortas
	public static final Integer SALTY_CAKE = 1;
	public static final Integer CHOCOLATE_CAKE = 2;
	public static final Integer CAKE_TOP = 3;
	public static final Integer CAKE_FILLING = 4;

	// Carnes
	public static final Integer PESTLE = 1;
	public static final Integer BREADED_STEAK = 2;
	public static final Integer ROAST_BEEF = 3;
	public static final Integer BARBECUE = 4;

	// Doce / Sobremesa
	public static final Integer MOUSSE = 1;
	public static final Integer PUDDING = 2;
	public static final Integer MILK_CANDY = 3;
	public static final Integer MILK_BRAID = 4;

	// Lanche
	public static final Integer SANDWICH = 1;
	public static final Integer DONUT = 2;
	public static final Integer GINGERBREAD = 3;
	public static final Integer BREAD = 4;
}
