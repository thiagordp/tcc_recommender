/**
 * 
 */
package com.ufsc.enc.tcc.recommender.util;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.ufsc.enc.tcc.recommender.database.MetaInformationBasis;
import com.ufsc.enc.tcc.recommender.database.MongoDB;
import com.ufsc.enc.tcc.recommender.model.EPC;

/**
 * 
 * @author trdp
 *
 */
public class GeneralOperations
{
	public static <K, V> Map<K, V> sortByValueDescending(Map<K, V> map)
	{
		List<Entry<K, V>> list = new LinkedList<>(map.entrySet());
		Collections.sort(list, new Comparator<Object>()
		{
			@SuppressWarnings("unchecked")
			public int compare(Object o1, Object o2)
			{
				return ((Comparable<V>) ((Map.Entry<K, V>) (o2)).getValue())
						.compareTo(((Map.Entry<K, V>) (o1)).getValue());
			}
		});

		Map<K, V> result = new LinkedHashMap<>();

		for (Iterator<Entry<K, V>> it = list.iterator(); it.hasNext();) {
			Map.Entry<K, V> entry = (Map.Entry<K, V>) it.next();
			result.put(entry.getKey(), entry.getValue());
		}

		return result;
	}
	
	public static <K, V> Map<K, V> sortByValueAscending(Map<K, V> map)
	{
		List<Entry<K, V>> list = new LinkedList<>(map.entrySet());
		Collections.sort(list, new Comparator<Object>()
		{
			@SuppressWarnings("unchecked")
			public int compare(Object o1, Object o2)
			{
				return ((Comparable<V>) ((Map.Entry<K, V>) (o1)).getValue())
						.compareTo(((Map.Entry<K, V>) (o2)).getValue());
			}
		});

		Map<K, V> result = new LinkedHashMap<>();

		for (Iterator<Entry<K, V>> it = list.iterator(); it.hasNext();) {
			Map.Entry<K, V> entry = (Map.Entry<K, V>) it.next();
			result.put(entry.getKey(), entry.getValue());
		}

		return result;
	}

	public static HashMap<String, Double> productListToHashMap(JSONObject jsonProductList)
	{
		HashMap<String, Double> hashProductList = new HashMap<>();

		JSONArray jsonArray;
		try {
			jsonArray = jsonProductList.getJSONArray(Constants.PRODUCT_LIST_KEY);

			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonTmp = jsonArray.getJSONObject(i);

				hashProductList.put(jsonTmp.getString(Constants.PRODUCT_BARCODE_KEY),
						Double.valueOf(jsonTmp.getString(Constants.PRODUCT_QUANTITY_KEY)));
			}

		}
		catch (JSONException e) {
			e.printStackTrace();
			hashProductList = null;
		}

		return hashProductList;
	}

	/**
	 * Retorna uma referência de timestamp deslocado X horas em relação ao horário atual.
	 * @param hours
	 * @return
	 */
	public static Timestamp getHoursAgoTimestamp(Integer hours)
	{

		Long subtractTime = (long) ((hours * 3600) * 1000);

		Date date = new Date();
		Timestamp timestamp = new Timestamp(date.getTime());

		//System.out.println(timestamp);
		timestamp.setTime(timestamp.getTime() - subtractTime);
		//System.out.println(timestamp);

		return timestamp;
	}

	/**
	 * 
	 * @return
	 */
	public static String getTimestamp()
	{
		Date date = new Date();
		Timestamp timestamp = new Timestamp(date.getTime());
		return timestamp.toString();
	}

	/**
	 * 
	 * @param timeNano
	 */
	public static void showTime(Long timeNano)
	{
		System.out.print("Tempo de execução:\t");

		Double nanoTime = Double.valueOf(timeNano);

		if (nanoTime > 1000 * 1000 * 1000) { // segundos
			nanoTime = nanoTime / (1000 * 1000 * 1000);
			System.out.format("%.2fs\n", nanoTime);
		}
		else if (nanoTime > 1000 * 1000) { //
			nanoTime = nanoTime / (1000 * 1000);
			System.out.format("%.2fms\n", nanoTime);
		}
		else if (nanoTime > 1000) {
			nanoTime = nanoTime / (1000);
			System.out.format("%.2fus\n", nanoTime);
		}
		else {
			System.out.format("%.2fns\n", nanoTime);
		}
	}

	/**
	 * 
	 * @param message
	 * @return
	 */
	public static JSONObject JSONMessage(String message)
	{
		JSONObject json = new JSONObject();

		try {
			json.put("message", message);
		}
		catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return json;
	}

	/**
	 * Seleciona os metadados a partir das interações
	 * @param listInteractions
	 * @return
	 */
	public static JSONObject getCurrentproductsByInteractions(List<JSONObject> listInteractions, MongoDB mongoDB)
	{
		MetaInformationBasis metaInformationBasis = new MetaInformationBasis(mongoDB);
		JSONObject jsonProducts = new JSONObject();
		JSONArray jsonArray = new JSONArray();
		HashSet<BigInteger> epcCodes = new HashSet<>();

		System.out.println("ListInt\t" + listInteractions);
		// Resgate dos códigos EPC da interação e inserção no conjunto Hash de código
		for (JSONObject json : listInteractions) {

			try {
				jsonArray = json.getJSONArray(Constants.EPC_CODES_KEY);

				for (int i = 0; i < jsonArray.length(); i++) {
					epcCodes.add(new BigInteger(jsonArray.getString(i)));
				}
			}
			catch (JSONException e) {
				e.printStackTrace();
			}
		}

		// Nesse ponto tem-se uma lista de todos os produtos que interagiu nesse tempo
		jsonArray = new JSONArray();

		// Pegar a metaInformação de cada produto.
		System.out.println("Epccodes\t" + epcCodes);

		for (BigInteger epcCode : epcCodes) {

			// Resgatar código de barras a partir do EPC
			EPC epc = new EPC(epcCode);

			String barCode = "";
			try {
				JSONObject jsonMeta = metaInformationBasis.getProductMetaInfoByEPC(epc.getEPCManagerNumber(),
						epc.getObjectClass());

				if (jsonMeta == null) {		// Caso não encontre, pula pro próximo epc.
					continue;
				}

				//System.out.println("JSON_META: " + jsonMeta.toString());
				barCode = jsonMeta.getString(Constants.PRODUCT_BARCODE_KEY);

				boolean found = false;

				// Verifica se já foi adicionado
				for (int i = 0; i < jsonArray.length(); i++) {

					JSONObject jsonObj = jsonArray.getJSONObject(i);

					if (jsonObj.getString(Constants.PRODUCT_BARCODE_KEY).equals(barCode)) {
						found = true;

						Integer quantity = jsonObj.getInt(Constants.PRODUCT_QUANTITY_KEY);
						jsonObj.put(Constants.PRODUCT_QUANTITY_KEY, quantity + 1);

						break;
					}
				}

				if (!found) {
					JSONObject json = new JSONObject();
					json.put(Constants.PRODUCT_BARCODE_KEY, barCode);
					json.put(Constants.PRODUCT_QUANTITY_KEY, 1);

					jsonArray.put(json);
				}

			}
			catch (JSONException e) {
				e.printStackTrace();
			}
		}

		try {
			jsonProducts.put(Constants.PRODUCT_LIST_KEY, jsonArray);
			System.out.println("PRODUCTS:\t" + jsonProducts);
		}
		catch (JSONException e) {
			e.printStackTrace();
		}

		return jsonProducts;
	}

	/**
	 * Seleciona os metadados a partir das interações
	 * @param listInteractions
	 * @return
	 */
	public static HashMap<String, Double> getCurrentproductsByInteractionsHash(List<JSONObject> listInteractions,
			MongoDB mongoDB)
	{
		HashMap<String, Double> products = new HashMap<>();

		MetaInformationBasis metaInformationBasis = new MetaInformationBasis(mongoDB);
		JSONObject jsonProducts = new JSONObject();
		JSONArray jsonArray = new JSONArray();
		HashSet<BigInteger> epcCodes = new HashSet<>();

		// Resgate dos códigos EPC da interação e inserção no conjunto Hash de código
		for (JSONObject json : listInteractions) {

			try {
				jsonArray = json.getJSONArray(Constants.EPC_CODES_KEY);

				for (int i = 0; i < jsonArray.length(); i++) {
					epcCodes.add(new BigInteger(jsonArray.getString(i)));
				}

			}
			catch (JSONException e) {
				e.printStackTrace();
			}
		}

		// Nesse ponto tem-se uma lista de todos os produtos que interagiu nesse tempo

		jsonArray = new JSONArray();

		// Pegar a metaInformação de cada produto.
		System.out.println(epcCodes);

		for (BigInteger epcCode : epcCodes) {

			// Resgatar código de barras a partir do EPC
			EPC epc = new EPC(epcCode);

			String barCode = "";
			try {
				JSONObject jsonMeta = metaInformationBasis.getProductMetaInfoByEPC(epc.getEPCManagerNumber(),
						epc.getObjectClass());

				if (jsonMeta == null) {		// Caso não encontre, pula pro próximo epc.
					continue;
				}

				//System.out.println("JSON_META: " + jsonMeta.toString());
				barCode = jsonMeta.getString(Constants.PRODUCT_BARCODE_KEY);
				Double prodQtt = jsonMeta.getDouble(Constants.PRODUCT_QUANTITY_KEY);

				boolean found = false;

				// Verifica se já foi adicionado
				for (String barcodeKey : products.keySet()) {

					//	JSONObject jsonObj = jsonArray.getJSONObject(i);

					if (barcodeKey.equals(barCode)) {
						found = true;

						Double quantity = products.get(barcodeKey);

						//jsonObj.put(Constants.PRODUCT_QUANTITY_KEY, quantity + 1); // como é referência vai funcionar
						products.put(barcodeKey, quantity + prodQtt);

						break;
					}
				}

				if (!found) {

					products.put(barCode, prodQtt);
				}

			}
			catch (JSONException e) {
				e.printStackTrace();
			}
		}

		//	jsonProducts.put(Constants.PRODUCT_LIST_KEY, jsonArray);
		System.out.println("PRODUCTS:\t" + products);

		return products;
	}

	public static HashMap<String, Double> getCurrentClassProductsByInteractionsHash(List<JSONObject> listInteractions,
			MongoDB mongoDB)
	{
		HashMap<String, Double> products = new HashMap<>();

		MetaInformationBasis metaInformationBasis = new MetaInformationBasis(mongoDB);

		JSONObject jsonProducts = new JSONObject();
		JSONArray jsonArray = new JSONArray();
		HashSet<BigInteger> epcCodes = new HashSet<>();

		// Resgate dos códigos EPC da interação e inserção no conjunto Hash de código
		for (JSONObject json : listInteractions) {

			try {
				jsonArray = json.getJSONArray(Constants.EPC_CODES_KEY);

				for (int i = 0; i < jsonArray.length(); i++) {
					epcCodes.add(new BigInteger(jsonArray.getString(i)));
				}

			}
			catch (JSONException e) {
				e.printStackTrace();
			}
		}

		// Nesse ponto tem-se uma lista de todos os produtos que interagiu nesse tempo

		jsonArray = new JSONArray();

		// Pegar a metaInformação de cada produto.
		System.out.println(epcCodes);

		for (BigInteger epcCode : epcCodes) {

			// Resgatar código de barras a partir do EPC
			EPC epc = new EPC(epcCode);
			Integer classProd = 0;
			Integer product = 0;
			String barCode = "";

			try {
				JSONObject jsonMeta = metaInformationBasis.getProductMetaInfoByEPC(epc.getEPCManagerNumber(),
						epc.getObjectClass());

				if (jsonMeta == null) {		// Caso não encontre, pula pro próximo epc.
					continue;
				}

				//System.out.println("JSON_META: " + jsonMeta.toString());
				barCode = jsonMeta.getString(Constants.PRODUCT_BARCODE_KEY);
				JSONObject classJSON = jsonMeta.getJSONObject(Constants.PRODUCT_CLASSIFICATION_KEY);

				classProd = classJSON.getInt(Constants.PRODUCT_CLASS_KEY);
				product = classJSON.getInt(Constants.PRODUCT_ESPECIFICATION_KEY);

				boolean found = false;

				// Verifica se já foi adicionado
				for (int i = 0; i < jsonArray.length(); i++) {

					JSONObject jsonObj = jsonArray.getJSONObject(i);
					JSONObject tmpClass = jsonObj.getJSONObject(Constants.PRODUCT_CLASSIFICATION_KEY);
					Integer classProdTmp = tmpClass.getInt(Constants.PRODUCT_CLASS_KEY);
					Integer productTmp = tmpClass.getInt(Constants.PRODUCT_ESPECIFICATION_KEY);

					// TODO: continuar daqui.

					if (classProdTmp.equals(classProd) && productTmp.equals(product)) {

						found = true;

						Integer quantity = jsonObj.getInt(Constants.PRODUCT_QUANTITY_KEY);
						jsonObj.put(Constants.PRODUCT_QUANTITY_KEY, quantity + 1);

						break;
					}
				}

				if (!found) {
					JSONObject json = new JSONObject();
					json.put(Constants.PRODUCT_BARCODE_KEY, barCode);
					json.put(Constants.PRODUCT_QUANTITY_KEY, 1);

					jsonArray.put(json);
				}

			}
			catch (JSONException e) {
				e.printStackTrace();
			}
		}

		try {
			jsonProducts.put(Constants.PRODUCT_LIST_KEY, jsonArray);
			System.out.println("PRODUCTS:\t" + jsonProducts);
		}
		catch (JSONException e) {
			e.printStackTrace();
		}

		return products;
	}

	public static String convertStreamToString(java.io.InputStream is)
	{
		String response = "";
		java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");

		response = s.hasNext() ? s.next() : "";
		return response;
	}

}
